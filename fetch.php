
<?php

//fetch.php

include("database_connection.php");

$query = "SELECT * FROM quotes"
        ." ORDER BY date DESC;";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$total_row = $statement->rowCount();
$output = '<div class="row">
';
if ($total_row > 0) {
    foreach ($result as $row) {
        $output .= '
	           <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <center><p class="card-text"><font face = "Arizonia" size =" 5">' . $row['quote'] . '</font></p></center>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                   <button type="button" name="delete" class="btn btn-sm btn-outline-secondary delete" id="' . $row["id"] . '">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
          		';
    }
} else {
    $output .= '
	<tr>
		<td colspan="4" align="center">Data not found</td>
	</tr>
	';
}
$output .= '</div>';
echo $output;
