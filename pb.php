
<?php

//fetch.php

include("database_connection.php");

$query = "SELECT * FROM quotes"
        . " ORDER BY date DESC;";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$total_row = $statement->rowCount();
$total_in_percent = $total_row * 10;
$output = ' <div class="progress-bar" role="progressbar" style="width: '.$total_in_percent.'%;" aria-valuenow="1" aria-valuemin="1" aria-valuemax="1">'.$total_row.' of 10</div>';
echo $output;