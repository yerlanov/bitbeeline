<html>  
    <head>  
        <title>Quotes</title>  
        <link rel="css/stylesheet" href="jquery-ui.css">
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script src="js/jquery-ui.js"></script>
        <link href="css/album.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
    </head>  
    <body>  

        <div class="pricing-header px-5 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <div class="progress" id="progress_bar">
            </div>
            <h1 class="display-4">Quote</h1>
            <form method="post" id="user_form">
                <div class="form-group">
                    <textarea class="form-control" id="quote" required="" name="quote" rows="3"></textarea>
                    <span id="error_quote" class="text-danger"></span>
                </div>
                <div class="form-group">
                    <input type="hidden" name="action" id="action" value="insert" />
                    <input type="submit" name="form_action" id="form_action" class="btn btn-info" value="Add Quote" />
                </div>
            </form>
        </div>


        <div class="container" id="user_data">

        </div>                

        <div id="action_alert" title="Action">

        </div>

        <div id="delete_confirmation" title="Confirmation">
            <p>Are you sure you want to Delete this Quote?</p>
        </div>

    </body>  
</html>  




<script>
    $(document).ready(function () {

        load_data();
        load_pb();

        function load_data()
        {
            $.ajax({
                url: "fetch.php",
                method: "POST",
                success: function (data)
                {
                    $('#user_data').html(data);
                }
            });
        }

        function load_pb()
        {
            $.ajax({
                url: "pb.php",
                method: "POST",
                success: function (data)
                {
                    $('#progress_bar').html(data);
                }
            });
        }

        $("#user_dialog").dialog({
            autoOpen: false,
            width: 400
        });

        $('#add').click(function () {
            $('#user_dialog').attr('title', 'Add Data');
            $('#action').val('insert');
            $('#form_action').val('Insert');
            $('#user_form')[0].reset();
            $('#form_action').attr('disabled', false);
            $("#user_dialog").dialog('open');
        });

        $('#user_form').on('submit', function (event) {
            event.preventDefault();
            $('#form_action').attr('disabled', 'disabled');
            var form_data = $(this).serialize();
            $.ajax({
                url: "action.php",
                method: "POST",
                data: form_data,
                success: function (data)
                {
                    $('#user_dialog').dialog('close');
                    $('#action_alert').html(data);
                    $('#action_alert').dialog('open');
                    load_data();
                    load_pb()
                    $('#quote').val('');
                    $('#form_action').attr('disabled', false);
                }
            });


        });

        $('#action_alert').dialog({
            autoOpen: false
        });

        $('#delete_confirmation').dialog({
            autoOpen: false,
            modal: true,
            buttons: {
                Ok: function () {
                    var id = $(this).data('id');
                    var action = 'delete';
                    $.ajax({
                        url: "action.php",
                        method: "POST",
                        data: {id: id, action: action},
                        success: function (data)
                        {
                            $('#delete_confirmation').dialog('close');
                            $('#action_alert').html(data);
                            $('#action_alert').dialog('open');
                            load_data();
                            load_pb()
                        }
                    });
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });

        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            $('#delete_confirmation').data('id', id).dialog('open');
        });

    });
</script>
